---
layout: job_family_page
title: "Contract Manager"
---

The Contract Manager is responsible for managing the contracting process for sales and non-sales engagements. At the direction of the Senior Director of Legal Affairs, the Contract Manager will review, negotiate, and manage all contracts, as well as oversee the overall contracting process. This is a remote role.

## Responsibilities

* Draft and negotiate a wide range of contracts including services, consulting, marketing, licensing, non-disclosure, data privacy, and other commercial and technology related agreements
* Work closely with members of the Legal team to develop and improve applicable standardized forms, processes, and procedures
* Perform risk analysis review for all contractual documents and recommend mitigating options
* Understand the Company and products in order to identify risks, develop solutions, mitigation and negotiation strategies
* Input completed contracts into the contract management system and capture key terms


## Requirements

* Minimum of 5 years of contracts drafting, reviewing and negotiation experience at a software technology company
* BA/BS required. JD, paralegal certificate, or equivalent work experience preferred.
* Expertise in drafting and negotiating a wide variety of commercial, licensing and other technology-related agreements.
* Understanding of data privacy requirements.
* Proactive, dynamic and result driven individual with strong attention to detail.
* Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization.
* Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.
* Superior analytical ability, project management experience, and communication skills
* Ability to manage internal customer priorities and needs.

## Performance Indicators
* [Number of Opportunities closed by Contract Manager(s) per Quarter](/handbook/legal/#number-of-opportunities-closed-by-contract-managers-per-quarter--66)
* [Response times to initial requests for review](/handbook/legal/#response-times-to-initial-requests-for-review--24-business-hours)
* [‘Turn-Around’ times on received red-lines](/handbook/legal/#turn-around-times-on-received-red-lines--72-business-hours)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our team page.

* Selected candidates will be invited to schedule a 45 min [screening call](/handbook/hiring/#screening-call) with our Global Recruiter.
* Next, candidates will be invited to schedule a first interview with our Senior Director of Legal Affairs.
* Candidates might at this point be invited to schedule with an additional team members.
* Successful candidates will subsequently be made an offer via email.
* Previous experience in a Global Start-up and remote first environment would be ideal.
* Successful completion of a background check.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
