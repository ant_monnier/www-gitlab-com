---
layout: handbook-page-toc
title: "Key Monthly Review"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Key Metrics Monthly Review

### Purpose

For each [executive](/company/team/structure/#executives) we have a monthly call to discuss the [Key Metrics](#key-metrics) of that department in order to:

1. Makes it much easier to stay up to date for everyone.
1. Be accountable to the rest of the company.
1. Understand month to month variances.
1. Understand against the plan, forecast and operating model.
1. Ensure there is tight connection between OKRs and KPIs.

Some executives will have additional calls in areas that report to them based on the number and importance of KPI/OKRs associated with the function.

### Key Metrics

1. [KPIs](/handbook/business-ops/data-team/metrics/) of that department
1. [OKRs](/company/okrs/) that are assigned to this executive.

### Timing

Meetings are monthly starting on the 10th day after month end.

### Invitees

Required invites are the executive and the CFO. Optional attendees are the rest of the e-team and anyone who has an interest in the metric.

### Meeting Format

1. The functional owner will prepare a google slide presentation with the content to be reviewed.
1. The finance business partner assigned to the functional area will meet with the owner at least one week in advance and ensure that follow-ups from last meeting have been completed and that data to be presented has proper definitions and is derived from a Single Source of Truth.
1. The title of every slide should be the key takeaway
1. A label on the slide should convey whether the metric result is "on-track" (green), "needs improvement" (yellow), or is an "urgent concern" (red).
1. A google doc will also be linked from the calendar invite for participants to log questions or comments for discussion, and to any additional track decisions & action items.
1. Wherever possible the metric being reviewed should be compared to Plan, OKR target, KPI target, or industry benchmark.
1. There is no presentation, the meeting is pure Q&A. Of course people can ask to talk them through a slide. If you want to present please [post a Youtube video](/handbook/communication/youtube/) like [Todd did](https://www.youtube.com/watch?v=hpyR39y_1d0) and link that from the slide deck, agenda, and/or slack.
1. The functional owner is responsible for preparing the document 24 hours advance of the meeting. The owner should update the meeting invite and send to all guests so they know the materials are ready for review.
1. A [blank template](https://docs.google.com/presentation/d/1lfQMEdSDc_jhZOdQ-TyoL6YQNg5Wo7s3F3m8Zi9NczI/edit) still needs labels

### Future

We want to get to reviewing a live dashboard in addition to having the data reside in Google Sheets.

