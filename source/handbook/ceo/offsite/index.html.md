---
layout: handbook-page-toc
title: "E-group offsite"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

The E-group offsite happens every quarter for four days.

## Goal

The goal is to have 25 or 50 minute discussions around topics that benefit from in-person dialogue, require more context and where the E-Group is likely to disagree. The agenda should include discussions that are:

1. Top of mind
1. Actionable
1. Impactful to the trajectory of the company
1. Cross-functional

## Notes

We take notes in a Google Doc that contains (a copy of) the agenda.
Please add links to relevant materials and proposals up front.
When there is an issue or doc linked we take notes there instead of in the overall doc.

## Follow up

We reserve 1/3 of the time for each subject to do the follow up.
Before we did this many conclusions never landed and/or resulted into action.
This can take the form of:

1. Merge request to the handbook
1. Create an issue
1. Schedule a meeting
1. Share notes with the rest of the company in Slack
1. Etc.

## Time management

If we can conclude a topic early we move on to one from a reserve list.
For each part of the day there is a chairperson who moves on, switches to followup, and leads other adjustments.

## Attendees

1. [Executives](/company/team/structure/#executives)
1. [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/); when not possible, the [Internal Strategy Consultant](/job-families/chief-executive-officer/internal-strategy-consultant/) will
1. [CEO Shadows](/handbook/ceo/shadow/)
1. [Executive Assistant](/job-families/people-ops/executive-assistant/) to the CEO (optional)

#### Roles

The **CEO Shadows** are responsible for [taking thorough notes](/handbook/ceo/offsite/#notes) throughout the event, so that the E-Group can be focused on the discussion. 
CEO Shadows will also tasked with making [Merge Requests](/handbook/ceo/offsite/#document-during-meeting) on behalf of an Executive. 
Please follow the below outlined process for announcing and merging the changes. 

The **Chief of Staff** or other other team member is reponsible for facilitating the event. 
They will work with the EBA closely to ensure the event runs smoothly. 
The CoS is the on-the-ground person ensuring that the event is kept on-schedule, discussions are kept on-subject, helping steer the conversation when necessary, guiding conversations towards action items, and ensuring that [implementation is about 50% of time](/handbook/ceo/offsite/#timeline). 

The **Executive Assistant to the CEO** is responsible for organizing and coordinating the Offsite, including travel, lodging, agendas, meals, and the book choice.

## Document During Meeting

We will document agreed changes directly to the handbook and any other relevant SSoT during the meeting.
Every item will get a MR maker and MR reviewer assigned.
Most of the time the MR maker will be the Chief of Staff, one of the CEO shadows, or the Executive Assistant.
When the MR is ready, the reviewer is at-mentioned in the public e-group channel in Slack.
The reviewer communicates with the maker via that Slack thread. 
The goal is to merge it the same day, preferably within 15 minutes.

## Logistics

Since most of the E-group is in the San Francisco Bay Area, we'll go to a location that is drivable or a short direct flight, for example: East Bay, Denver, Sonoma. We tend to pick a location at or close to a member of the e-group. 

## Schedule 

The off-site is a quarterly meeting scheduled over 4 days (including travel time). 
The meeting should take place during the second or third month of the quarter to avoid conflicts with Sales QBRs and ideally occur before quarterly Board of Director meetings. 
Scheduling for the event generally follows: 
1. Monday: travel day with optional dinner around 7pm local time
1. Tuesday: Full day meeting starting with breakfast at 8am
1. Wednesday: Full day meeting starting with breakfast at 8am. When the event does not coincide with Contribute, EA to the CEO will coordinate an off-site activity for the afternoon.
1. Thursday Half day meeting starting with breakfast at 8am. Usual end time is 12pm with departure flights scheduled in the late afternoon.

E-Group is welcome to fly in early or stay later pending their travel preferences. 

## Timeline

1. Plan
1. Gathering subjects - if a subject requires data to support the discussion, make a request of the data team for assistance no less than 2 weeks before the offsite
1. During Meeting
   1. Review previous quarter meeting MRs and Results
   1. Discussion: 50% of allotted time
   1. Merge Requests / Implementation: 50% of allotted time
   1. Data Team member on-call to respond to data requests during meeting

## Organization

The Executive Assistant to the CEO is responsible for organizing this. 
