---
layout: handbook-page-toc
title: Growth Section
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

For more details about the product vision for Growth, see our [Growth] Product Vision page.

[Growth]: /direction/growth/

## September 2019 Fast Boot

The Acquisition, Expansion, Conversion and Retention groups took part in a [Fast Boot](/handbook/engineering/fast-boot/) in September 2019.
[The planning issue](https://gitlab.com/gitlab-org/growth/product/issues/1) contains the proposal for Fast Boot,
and outcomes are available in the [Growth Fast Boot Page](/handbook/engineering/development/growth/fast-boot).

## Mission

The Growth section consists of groups that eliminate barriers between our users and product value.

**Managing customer licensing and transactions**

The [Fulfillment](/handbook/engineering/development/growth/fulfillment/) Group is responsible for:

Licensing
- Seamless license compliance for customers and GitLab Team Members

Transactions
- Make doing business with GitLab  efficient, easy, and a terrific user experience

**Deliver telemetry data that improves our product**

The [Telemetry](/handbook/engineering/development/growth/telemetry/) Group is responsible for:

Collection
- Collect information from GitLab.com and self-managed instances to improve our product
- Respect our users' privacy

Analysis
- Analyze and visualize collected data
- Operationalize what we’re collecting for the benefit of GitLab and our customers


**Drive value for the business and our users by improving activation, retention, upsell, and per-stage adoption**

We focus on validating ideas with data across the following four Groups:

[Acquisition Group](/handbook/engineering/development/growth/acquisition/)

- Get users off to a successful start
- Increase users completing key actions

[Conversion Group](/handbook/engineering/development/growth/conversion/)
- Encourage cross-stage usage
- Increase Stage Monthly Active Users

[Expansion Group](/handbook/engineering/development/growth/expansion/)
- Drive adoption of “higher” tiers
- Increase revenue per user/transaction

[Retention Group](/handbook/engineering/development/growth/retention/)
- Get customers and users to return
- Increase gross retention

We work closely with the [Data Team](/handbook/business-ops/data-team/) along with our [Product Team](/handbook/product/categories/#growth-stage)
counterparts to design and implement experiments that measure the impact of changes to our messaging, UX, and overall experience of using GitLab.

**Support and improve the GitLab Handbook**

The [Handbook Team](/handbook/engineering/development/growth/handbook-team/) is responsible for both the editing experience and the reading experience of our [Handbook](/handbook/).

## Section Members

The following people are permanent members of the Growth Section:

<%
departments = ['Acquisition', 'Conversion', 'Expansion' , 'Retention', 'Fulfillment', 'Telemetry']
department_regexp = /(#{Regexp.union(departments)})/
%>

<%=  direct_team(role_regexp: department_regexp, manager_role: 'Director of Engineering, Growth') %>

## All Team Members

The following people are permanent members of groups that belong to the Growth Section:

### Acquisition and Conversion
<%= direct_team(manager_role:  "Engineering Manager, Acquisition and Conversion") %>

### Expansion and Retention
<%= direct_team(manager_role:  "Engineering Manager, Expansion and Retention") %>

### Fulfillment Back End
<%= direct_team(manager_role:  "Engineering Manager, Fulfillment and Telemetry") %>

### Fulfillment Front End
<%= direct_team(manager_role:  "Frontend Engineering Manager, Fulfillment") %>


## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Growth/, direct_manager_role: 'Engineering Manager, Growth') %>


## How We work
As part of the wider Growth stage we track and work on issues with the label `~"devops::growth"`.

### Growth Issues Board
The [Growth issues board] shows `~"devops::growth"` issues by workflow state,
and can be filtered by additional `group::` label:
* [Growth issues board] unfiltered
* [`~"growth::aquisition"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aacquisition)
* [`~"growth::conversion"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aconversion)
* [`~"growth::expansion"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aexpansion)
* [`~"growth::retention"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Aretention)
* [`~"growth::fulfillment"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Afulfillment)
* [`~"growth::telemetry"`](https://gitlab.com/groups/gitlab-org/-/boards/1158847?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Agrowth&label_name%5B%5D=group%3A%3Atelemetry)

This board uses the [Product Development Workflow](/handbook/engineering/workflow/#workflow-labels) labels for issues where applicable.

### Experiment workflow Labels

[Growth - Experiments] have the `~"growth experiment"` label added. For tracking the status of experiments we also use the following `experiment::` workflow labels:
* `~"experiment::active"`
* `~"experiment::blocked"`
* `~"experiment::validated"`
* `~"experiment::invalidated"`
* `~"experiment::inconclusive"`
* `~"experiment::validated::clean_up_for_production"`

This workflow can be tracked on the [Growth - Experiments] board.

### UX Workflows
We follow the UX Team's [Product Designer](/handbook/engineering/ux/ux-designer/#product-designer)
and [Researcher](/handbook/engineering/ux/ux-research/) workflows.
We also have a Growth specific workflow that you can read about [here](/handbook/engineering/ux/stage-group-ux-strategy/growth/#how-we-work).

## Common Links

* [Growth section]
* [Growth issues board]
* `#s_growth` in [Slack](https://gitlab.slack.com/archives/s_growth)
* [Growth Performance Indicators]
* [Fulfillment issues board]
* `#g_fulfillment` in [Slack](https://gitlab.slack.com/archives/g_fulfillment)
* [Telemetry issues board]
* `#g_telemetry` in [Slack](https://gitlab.slack.com/archives/g_telemetry)
* [Growth opportunities]
* [Growth meetings and agendas]
* [GitLab values]

[GitLab values]: /handbook/values/
[Growth section]: /handbook/engineering/development/growth/
[Growth issues board]: https://gitlab.com/groups/gitlab-org/-/boards/1158847
[Growth - Experiments]: https://gitlab.com/groups/gitlab-org/-/boards/1352542
[Growth opportunities]: https://gitlab.com/gitlab-org/growth/product
[Growth meetings and agendas]: https://docs.google.com/document/d/1QB9uVQQFuKhqhPkPwvi48GaibKDwGAfKKqcF-s3Y6og

[Growth Performance Indicators]: /handbook/engineering/development/growth/performance-indicators/

[Fulfillment issues board]: https://gitlab.com/gitlab-org/fulfillment/issues
[Telemetry issues board]: https://gitlab.com/gitlab-org/telemetry/issues
